# takin-sys

#### 介绍
 :couplekiss: takin框架集成大多数互联网功能：统一异常、swagger、软删除、签名验签、分页封装、token验证、权限验证、日志集成、配置中心、服务注册发现、代码生成器maven插件是一个时候中小型企业开发的框架。<br>
 :dancers: 作者不怎么会写文档，有疑问请留言。<br>
 :two_women_holding_hands: 前后端分离<br>
 :family: 一直想做后台管理系统，这是一套全部开源的快速开发平台，毫无保留提供给大家免费使用。<br>

#### 演示地址
http://117.50.181.140:6688<br>
登录账号密码为admin/admin【请勿修改权限密码】<br>

#### 技术选型
spring boot,nacos,redis,RPC,mybatis-plus,mysql,swagger等

#### 前端代码
https://gitee.com/zhangchenyan/takin-sys-vue.git<br>
登录账号密码为admin/admin

#### 使用说明
0. 请优先阅读 :elephant:  takin框架说明 https://gitee.com/zhangchenyan/takin.git
1. 安装mysql,导入config sql文件。
2. 安装redis
3. 安装nacos,导入config nacos配置，根据实际情况修改数据库配置和redis配置。
4. 启动程序如图。<br>
![输入图片说明](img/1.png)

#### 功能描述
-使用takin开发框架 搭建的通用权限(RBAC)后台管理系统<br>
-支持RBAC角色权限,支持前端按钮权限<br>
#### 功能截图
![img.png](img/2.png)
![img.png](img/3.png)
![img.png](img/4.png)
![img.png](img/5.png)
![img.png](img/6.png)
![img.png](img/7.png)
![img.png](img/8.png)
![img.png](img/9.png)