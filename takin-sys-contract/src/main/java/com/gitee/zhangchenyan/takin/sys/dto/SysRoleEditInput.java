package com.gitee.zhangchenyan.takin.sys.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@ApiModel(value = "SysRole对象", description = "SysRole修改")
public class SysRoleEditInput {

    private long id;

    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("角色标识")
    private String code;

    @ApiModelProperty("描述")
    private String describes;

    @ApiModelProperty("排序")
    private BigDecimal sort;

    @ApiModelProperty("0无效，1有效")
    private Boolean status;

    @ApiModelProperty("SysMenu id集合")
    private List<Long> menuIdList;
}
