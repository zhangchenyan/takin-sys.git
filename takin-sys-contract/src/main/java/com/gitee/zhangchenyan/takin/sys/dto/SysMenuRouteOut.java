package com.gitee.zhangchenyan.takin.sys.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

/**
 * @Deacription vue-next-admin菜单路由
 * @Author zl
 * @Date 2022/3/14 20:51
 * @Version 1.0
 **/
@ApiModel("vue-next-admin菜单路由")
@Data
public class SysMenuRouteOut {
    private int type;
    private String path;
    private String name;
    private String component;
    private String redirect;
    private SysMenuMetaOut meta;
    private List<SysMenuRouteOut> children;
}
