package com.gitee.zhangchenyan.takin.sys.service;

import com.gitee.zhangchenyan.takin.sys.entity.SysUser;

public interface ITestService {
    SysUser getById(Long id);

    void test();
}
