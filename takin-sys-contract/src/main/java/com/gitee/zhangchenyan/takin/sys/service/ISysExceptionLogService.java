package com.gitee.zhangchenyan.takin.sys.service;


import com.gitee.zhangchenyan.takin.sys.entity.SysExceptionLog;
import com.gitee.zhangchenyan.takin.service.BaseService;
import com.gitee.zhangchenyan.takin.sys.page.SysExceptionLogPageParam;
import java.util.List;

/**
* <p>
    * 服务类
    * </p>
*
* @author zl
* @since 2022-03-13
*/
public interface ISysExceptionLogService extends BaseService<SysExceptionLog, SysExceptionLogPageParam> {

}
