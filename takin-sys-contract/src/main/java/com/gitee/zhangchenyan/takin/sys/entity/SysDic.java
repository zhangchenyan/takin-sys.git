package com.gitee.zhangchenyan.takin.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.gitee.zhangchenyan.takin.service.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author zl
 * @since 2022-03-19
 */
@Getter
@Setter
@TableName("sys_dic")
@ApiModel(value = "SysDic对象", description = "")
public class SysDic extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("字典编码")
    private String code;

    @ApiModelProperty("字典名称")
    private String name;

    @ApiModelProperty("字典描述")
    private String describes;

    @ApiModelProperty("字段集合")
    private String content;

    @ApiModelProperty("0未启用，1启动")
    private Boolean status;


}
