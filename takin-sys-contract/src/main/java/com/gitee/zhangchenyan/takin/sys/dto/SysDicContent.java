package com.gitee.zhangchenyan.takin.sys.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "SysDicContent", description = "SysDicContent")
public class SysDicContent {
    private String key;
    private String value;
}
