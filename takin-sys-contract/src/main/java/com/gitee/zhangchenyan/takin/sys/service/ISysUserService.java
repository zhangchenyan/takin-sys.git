package com.gitee.zhangchenyan.takin.sys.service;

import com.gitee.zhangchenyan.takin.service.PageResult;
import com.gitee.zhangchenyan.takin.sys.dto.SysUserOut;
import com.gitee.zhangchenyan.takin.sys.page.SysUserPageParam;
import com.gitee.zhangchenyan.takin.sys.entity.SysUser;
import com.gitee.zhangchenyan.takin.service.BaseService;

/**
 * <p>
 * 系统用户 服务类
 * </p>
 *
 * @author zl
 * @since 2022-02-25
 */
public interface ISysUserService extends BaseService<SysUser, SysUserPageParam> {
    PageResult<SysUserOut> getPageVo(SysUserPageParam pageParam);
}
