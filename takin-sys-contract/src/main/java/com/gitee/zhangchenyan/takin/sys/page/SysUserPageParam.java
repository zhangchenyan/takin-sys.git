package com.gitee.zhangchenyan.takin.sys.page;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.gitee.zhangchenyan.takin.service.BasePageParam;

@Data
@ApiModel("SysUser分页页查询入参")
public class SysUserPageParam extends BasePageParam {
    @ApiModelProperty("昵称")
    private String nickName;
}
