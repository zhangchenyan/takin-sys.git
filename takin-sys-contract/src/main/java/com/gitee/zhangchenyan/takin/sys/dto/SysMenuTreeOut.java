package com.gitee.zhangchenyan.takin.sys.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.gitee.zhangchenyan.takin.common.utils.BaseTree;

@ApiModel("参数树形出参")
@EqualsAndHashCode(callSuper = true)
@Data
public class SysMenuTreeOut extends BaseTree<SysMenuTreeOut> {
    
    @ApiModelProperty("1目录,2菜单,3按钮")
    private Integer type;

    @ApiModelProperty("菜单名称国际化")
    private String title;

    private String path;

    @ApiModelProperty("路由名称")
    private String name;

    @ApiModelProperty("重定向")
    private String redirect;

    @ApiModelProperty("component路径")
    private String component;

    @ApiModelProperty("外链/内嵌链接地址")
    private String link;

    @ApiModelProperty("菜单显示-1隐藏0不隐藏")
    private Boolean hide;

    @ApiModelProperty("页面缓存-1缓存0不缓存")
    private Boolean keepAlive;

    @ApiModelProperty("菜单是否固定（固定在 tagsView 中，不可进行关闭），右键菜单无 `关闭` 项	1固定0不固定")
    private Boolean affix;

    @ApiModelProperty("1外链")
    private Boolean isLink;

    @ApiModelProperty("1内链")
    private Boolean isIframe;

    @ApiModelProperty("菜单图标")
    private String icon;

    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("后端权限")
    private String permission;


}
