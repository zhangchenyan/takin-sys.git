package com.gitee.zhangchenyan.takin.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import com.gitee.zhangchenyan.takin.service.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 角色菜单关联
 * </p>
 *
 * @author zl
 * @since 2022-04-15
 */
@Getter
@Setter
@TableName("sys_roles_menus")
@ApiModel(value = "SysRolesMenus对象", description = "角色菜单关联")
public class SysRolesMenus extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("角色ID")
    private Long roleId;

    @ApiModelProperty("菜单ID")
    private Long menuId;


}
