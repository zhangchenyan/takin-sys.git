package com.gitee.zhangchenyan.takin.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.gitee.zhangchenyan.takin.service.BaseEntity;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author zl
 * @since 2022-05-03
 */
@Getter
@Setter
@TableName("sys_exception_log")
@ApiModel(value = "SysExceptionLog对象", description = "")
public class SysExceptionLog extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("级别")
    private String level;

    @ApiModelProperty("异常名称")
    private String name;

    private String toString;

    private String message;

    private String stackTrace;


}
