package com.gitee.zhangchenyan.takin.sys.service;

import com.gitee.zhangchenyan.takin.sys.entity.SysDic;
import com.gitee.zhangchenyan.takin.service.BaseService;
import com.gitee.zhangchenyan.takin.sys.page.SysDicPageParam;

/**
* <p>
    * 服务类
    * </p>
*
* @author zl
* @since 2022-03-13
*/
public interface ISysDicService extends BaseService<SysDic, SysDicPageParam> {

}
