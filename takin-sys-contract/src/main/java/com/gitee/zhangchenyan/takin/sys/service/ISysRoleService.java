package com.gitee.zhangchenyan.takin.sys.service;


import com.gitee.zhangchenyan.takin.common.result.Result;
import com.gitee.zhangchenyan.takin.sys.dto.SysCommonDelInput;
import com.gitee.zhangchenyan.takin.sys.dto.SysRoleAddInput;
import com.gitee.zhangchenyan.takin.sys.dto.SysRoleEditInput;
import com.gitee.zhangchenyan.takin.sys.entity.SysRole;
import com.gitee.zhangchenyan.takin.service.BaseService;
import com.gitee.zhangchenyan.takin.sys.entity.SysUser;
import com.gitee.zhangchenyan.takin.sys.page.SysRolePageParam;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zl
 * @since 2022-03-13
 */
public interface ISysRoleService extends BaseService<SysRole, SysRolePageParam> {
    Result addRoleAndRoleMenu(SysRoleAddInput sysRoleAddInput);

    Result editRoleAndRoleMenu(SysRoleEditInput sysRoleEditInput);

    Result removeByIds(SysCommonDelInput commonDelInput);

    /**
     * 根据用户ID查询角色集合
     * @param user 用户
     * @return 角色集合
     */
    List<String> getRoleByUserId(SysUser user);
}
