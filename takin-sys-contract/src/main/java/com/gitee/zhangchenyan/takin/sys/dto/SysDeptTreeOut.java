package com.gitee.zhangchenyan.takin.sys.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.gitee.zhangchenyan.takin.common.utils.BaseTree;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
public class SysDeptTreeOut extends BaseTree<SysDeptTreeOut> {


    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("描述")
    private String describes;

    @ApiModelProperty("负责人")
    private String person;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("排序")
    private BigDecimal sort;

    @ApiModelProperty("0禁用1启用")
    private Boolean status;

    @ApiModelProperty("创建时间")
    private LocalDateTime creationTime;

    @ApiModelProperty("最后修改时间")
    private LocalDateTime lastModificationTime;

    @ApiModelProperty("创建人ID")
    private Long creatorId;

    @ApiModelProperty("最后修改人ID")
    private Long lastModifierId;

    @ApiModelProperty("备注")
    private String remark;

}
