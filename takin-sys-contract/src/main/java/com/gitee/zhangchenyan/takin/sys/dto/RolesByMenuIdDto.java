package com.gitee.zhangchenyan.takin.sys.dto;

import lombok.Data;

@Data
public class RolesByMenuIdDto {
    private long menuId;
    private String roleCode;
}
