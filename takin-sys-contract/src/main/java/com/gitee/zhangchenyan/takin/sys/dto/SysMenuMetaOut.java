package com.gitee.zhangchenyan.takin.sys.dto;

import com.alibaba.fastjson.JSONArray;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

/**
 * @Deacription vue-next-admin菜单路由
 * @Author zl
 * @Date 2022/3/14 20:51
 * @Version 1.0
 **/
@ApiModel("vue-next-admin菜单路由")
@Data
public class SysMenuMetaOut {
    private String title;
    private String isLink;
    private Boolean isHide;
    private Boolean isKeepAlive;
    private Boolean isAffix;
    private Boolean isIframe;
    private List<String> roles;
    private String icon;
}
