package com.gitee.zhangchenyan.takin.sys.page;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.gitee.zhangchenyan.takin.service.BasePageParam;

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel("SysDic分页页查询入参")
public class SysDicPageParam extends BasePageParam {
    @ApiModelProperty("字典名称")
    private String name;
}
