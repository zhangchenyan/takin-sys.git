package com.gitee.zhangchenyan.takin.sys.service;


import com.gitee.zhangchenyan.takin.common.result.Result;
import com.gitee.zhangchenyan.takin.sys.dto.SysMenuTreeOut;
import com.gitee.zhangchenyan.takin.sys.entity.SysRolesMenus;
import com.gitee.zhangchenyan.takin.service.BaseService;
import com.gitee.zhangchenyan.takin.sys.page.SysRolesMenusPageParam;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zl
 * @since 2022-03-13
 */
public interface ISysRolesMenusService extends BaseService<SysRolesMenus, SysRolesMenusPageParam> {
    /**
     * 根据roleId查询menuId
     */
    Result<List<Long>> getMenuByRoleId(Long roleId);
    /**
     * 根据用户ID查询权限集合
     * @param userId 用户ID
     * @return 权限集合
     */
    List<String> getPermissionsByUserId(Long userId);

    /**
     * 根据用户ID查询按钮权限集合
     * @param userId 用户ID
     * @return 按钮权限集合
     */
    List<String> getBtnByUserId(Long userId);


}
