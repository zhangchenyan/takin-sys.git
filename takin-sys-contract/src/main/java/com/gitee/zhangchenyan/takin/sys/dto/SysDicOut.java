package com.gitee.zhangchenyan.takin.sys.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import com.gitee.zhangchenyan.takin.service.BaseEntity;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "SysDicOut对象", description = "")
public class SysDicOut extends BaseEntity {
    @ApiModelProperty("字典编码")
    private String code;

    @ApiModelProperty("字典名称")
    private String name;

    @ApiModelProperty("字典描述")
    private String describes;

    @ApiModelProperty("字段集合")
    private List<SysDicContent> content;

    @ApiModelProperty("0未启用，1启动")
    private Boolean status;
}
