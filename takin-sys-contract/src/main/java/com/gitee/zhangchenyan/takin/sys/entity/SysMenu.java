package com.gitee.zhangchenyan.takin.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.gitee.zhangchenyan.takin.service.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author zl
 * @since 2022-03-14
 */
@Getter
@Setter
@TableName("sys_menu")
@ApiModel(value = "SysMenu对象", description = "")
public class SysMenu extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private Long pid;

    @ApiModelProperty("1目录,2菜单,3按钮")
    private Integer type;

    @ApiModelProperty("菜单名称国际化")
    private String title;

    private String path;

    @ApiModelProperty("路由名称")
    private String name;

    @ApiModelProperty("重定向")
    private String redirect;

    @ApiModelProperty("component路径")
    private String component;

    @ApiModelProperty("外链/内嵌链接地址")
    private String link;

    @ApiModelProperty("菜单显示-1隐藏0不隐藏")
    private Boolean hide;

    @ApiModelProperty("页面缓存-1缓存0不缓存")
    private Boolean keepAlive;

    @ApiModelProperty("菜单是否固定（固定在 tagsView 中，不可进行关闭），右键菜单无 `关闭` 项	1固定0不固定")
    private Boolean affix;

    @ApiModelProperty("1外链")
    private Boolean isLink;

    @ApiModelProperty("1内链")
    private Boolean isIframe;

    @ApiModelProperty("菜单图标")
    private String icon;

    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("后端权限")
    private String permission;


}
