package com.gitee.zhangchenyan.takin.sys.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @Deacription 添加菜单如参数
 * @Author zl
 * @Date 2022/3/13 22:09
 * @Version 1.0
 **/
@Data
@ApiModel("添加菜单如参数")
public class SysMenuAddInput {
    private String pid;
    private String redirect;
    private String icon;
    private String permission;
    private Integer sort;
    private Integer type;
    private String title;
    private String path;
    private String component;
    private String name;
    private String link;
    private Boolean isLink;
    private Boolean isIframe;
    private Boolean keepAlive;
    private Boolean hide;
    private Boolean affix;
}
