package com.gitee.zhangchenyan.takin.sys.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;


@Data
@ApiModel("删除入参")
public class SysCommonDelInput {
    @ApiModelProperty("id")
    private List<Long> id;
}
