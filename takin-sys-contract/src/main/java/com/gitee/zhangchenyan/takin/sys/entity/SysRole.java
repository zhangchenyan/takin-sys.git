package com.gitee.zhangchenyan.takin.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import com.gitee.zhangchenyan.takin.service.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author zl
 * @since 2022-04-10
 */
@Getter
@Setter
@TableName("sys_role")
@ApiModel(value = "SysRole对象", description = "角色表")
public class SysRole extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("角色标识")
    private String code;

    @ApiModelProperty("描述")
    private String describes;

    @ApiModelProperty("排序")
    private BigDecimal sort;

    @ApiModelProperty("0无效，1有效")
    private Boolean status;


}
