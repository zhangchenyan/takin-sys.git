package com.gitee.zhangchenyan.takin.sys.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@ApiModel("用户查询出参")
public class SysUserOut {

    private Long id;

    @ApiModelProperty("部门名称")
    private String deptName;

    @ApiModelProperty("部门ID")
    private Long deptId;

    @ApiModelProperty("拥有角色")
    private String role;

    @ApiModelProperty("拥有角色")
    private String roleName;

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("昵称")
    private String nickName;

    @ApiModelProperty("性别")
    private String gender;

    @ApiModelProperty("手机号码")
    private String phone;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("创建时间")
    private LocalDateTime creationTime;
}
