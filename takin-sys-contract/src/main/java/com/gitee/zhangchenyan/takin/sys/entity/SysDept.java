package com.gitee.zhangchenyan.takin.sys.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import com.gitee.zhangchenyan.takin.service.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 部门
 * </p>
 *
 * @author zl
 * @since 2022-03-27
 */
@Getter
@Setter
@TableName("sys_dept")
@ApiModel(value = "SysDept对象", description = "部门")
public class SysDept extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ApiModelProperty("上级部门")
    private Long pid;

    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("描述")
    private String describes;

    @ApiModelProperty("负责人")
    private String person;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("排序")
    private BigDecimal sort;

    @ApiModelProperty("0禁用1启用")
    private Boolean status;


}
