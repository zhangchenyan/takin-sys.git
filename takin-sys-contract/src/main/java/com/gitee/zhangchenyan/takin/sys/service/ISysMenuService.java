package com.gitee.zhangchenyan.takin.sys.service;

import com.gitee.zhangchenyan.takin.common.result.Result;
import com.gitee.zhangchenyan.takin.service.BaseService;
import com.gitee.zhangchenyan.takin.sys.dto.SysCommonDelInput;
import com.gitee.zhangchenyan.takin.sys.dto.SysMenuAddInput;
import com.gitee.zhangchenyan.takin.sys.dto.SysMenuRouteOut;
import com.gitee.zhangchenyan.takin.sys.dto.SysMenuTreeOut;
import com.gitee.zhangchenyan.takin.sys.entity.SysMenu;
import com.gitee.zhangchenyan.takin.sys.page.SysMenuPageParam;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zl
 * @since 2022-03-13
 */
public interface ISysMenuService extends BaseService<SysMenu, SysMenuPageParam> {
    Result del(SysCommonDelInput sysCommonDelInput);

    void addMenu(SysMenuAddInput sysMenuAddInput);

    List<SysMenuRouteOut> getRoutes();

    List<SysMenuTreeOut> getMenu();

}
