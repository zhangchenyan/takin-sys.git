package com.gitee.zhangchenyan.takin.sys.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "SysDic对象", description = "SysDic修改")
public class SysDicEditInput {

    @ApiModelProperty("id")
    private Long id;

    @ApiModelProperty("字典编码")
    private String code;

    @ApiModelProperty("字典名称")
    private String name;

    @ApiModelProperty("字典描述")
    private String describes;

    @ApiModelProperty("字段集合")
    private List<SysDicContent> content;

    @ApiModelProperty("0未启用，1启动")
    private Boolean status;
}
