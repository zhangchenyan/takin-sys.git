package com.gitee.zhangchenyan.takin.sys.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author zl
 * @Date 2022/4/16 21:31
 * @Version 1.0
 **/
@Data
@ApiModel(value = "SysUser对象", description = "SysUser添加")
public class SysUserAddInput {
    @ApiModelProperty("部门名称")
    private Long deptId;

    @ApiModelProperty("用户名")
    private String username;

    @ApiModelProperty("昵称")
    private String nickName;

    @ApiModelProperty("性别")
    private String gender;

    @ApiModelProperty("手机号码")
    private String phone;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("密码")
    private String password;

    @ApiModelProperty("拥有角色")
    private String role;
}
