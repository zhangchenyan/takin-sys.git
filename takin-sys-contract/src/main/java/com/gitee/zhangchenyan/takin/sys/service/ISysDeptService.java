package com.gitee.zhangchenyan.takin.sys.service;

import com.gitee.zhangchenyan.takin.common.result.Result;
import com.gitee.zhangchenyan.takin.service.BaseService;
import com.gitee.zhangchenyan.takin.sys.dto.SysCommonDelInput;
import com.gitee.zhangchenyan.takin.sys.page.SysDeptPageParam;
import com.gitee.zhangchenyan.takin.sys.entity.SysDept;

/**
 * <p>
 * 部门 服务类
 * </p>
 *
 * @author zl
 * @since 2022-02-25
 */
public interface ISysDeptService extends BaseService<SysDept, SysDeptPageParam> {
    Result del(SysCommonDelInput sysCommonDelInput);
}
