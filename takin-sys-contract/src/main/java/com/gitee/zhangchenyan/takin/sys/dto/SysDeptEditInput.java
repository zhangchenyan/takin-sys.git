package com.gitee.zhangchenyan.takin.sys.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel("部门修改")
public class SysDeptEditInput {

    private long id;

    @ApiModelProperty("上级部门")
    private Long pid;

    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("描述")
    private String describes;

    @ApiModelProperty("负责人")
    private String person;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("排序")
    private BigDecimal sort;

    @ApiModelProperty("0禁用1启用")
    private Boolean status;
}
