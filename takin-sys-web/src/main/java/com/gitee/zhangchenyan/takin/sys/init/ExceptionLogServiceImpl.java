package com.gitee.zhangchenyan.takin.sys.init;

import com.gitee.zhangchenyan.takin.common.result.ExceptionLog;
import com.gitee.zhangchenyan.takin.service.IExceptionLogService;
import com.gitee.zhangchenyan.takin.service.client.ServiceClient;
import com.gitee.zhangchenyan.takin.sys.entity.SysExceptionLog;
import com.gitee.zhangchenyan.takin.sys.service.ISysExceptionLogService;
import org.springframework.stereotype.Component;

/**
 * @Deacription takin异常日志实现
 * @Author zl
 * @Date 2022/5/3 15:41
 * @Version 1.0
 **/
@Component
public class ExceptionLogServiceImpl implements IExceptionLogService {
    private final ISysExceptionLogService sysExceptionLogService = ServiceClient.create(ISysExceptionLogService.class);
    @Override
    public void save(ExceptionLog exceptionLog) {
        SysExceptionLog sysExceptionLog = new SysExceptionLog();
        sysExceptionLog.setLevel(exceptionLog.getLevel());
        sysExceptionLog.setName(exceptionLog.getName());
        sysExceptionLog.setToString(exceptionLog.getToString());
        sysExceptionLog.setMessage(exceptionLog.getMessage());
        sysExceptionLog.setStackTrace(exceptionLog.getStackTrace());
        sysExceptionLogService.save(sysExceptionLog);
    }
}
