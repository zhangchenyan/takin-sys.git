package com.gitee.zhangchenyan.takin.sys.controller;

import com.gitee.zhangchenyan.takin.auth.AuthCheckLogin;
import com.gitee.zhangchenyan.takin.auth.AuthCheckPermission;
import com.gitee.zhangchenyan.takin.common.result.Result;
import com.gitee.zhangchenyan.takin.common.result.ResultUtils;
import com.gitee.zhangchenyan.takin.service.PageResult;
import com.gitee.zhangchenyan.takin.service.client.ServiceClient;
import com.gitee.zhangchenyan.takin.sys.dto.SysCommonDelInput;
import com.gitee.zhangchenyan.takin.sys.entity.SysExceptionLog;
import com.gitee.zhangchenyan.takin.sys.page.SysExceptionLogPageParam;
import com.gitee.zhangchenyan.takin.sys.service.ISysExceptionLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



/**
 * @Deacription 异常日志管理
 * @Author zl
 * @Date 2022/3/12 21:13
 * @Version 1.0
 **/
@RestController
@Api(tags = "异常日志管理")
@RequestMapping("/takin_api/v1/sys/exceptionLog")
public class SysExceptionLogController {

    private final ISysExceptionLogService sysExceptionLogService = ServiceClient.create(ISysExceptionLogService.class);

    @AuthCheckLogin
    @AuthCheckPermission("sys:exceptionLog:index")
    @PostMapping("getPage")
    @ApiOperation("查询分页")
    public Result<PageResult<SysExceptionLog>> getPage(@RequestBody SysExceptionLogPageParam pageParam) {
        PageResult<SysExceptionLog> pageResult = sysExceptionLogService.getPage(pageParam);
        return ResultUtils.success(pageResult);
    }

    @AuthCheckLogin
    @AuthCheckPermission("sys:exceptionLog:del")
    @PostMapping("del")
    @ApiOperation("删除")
    public Result del(@RequestBody SysCommonDelInput sysCommonDelInput) {
        boolean saveSuccess = sysExceptionLogService.removeByIds(sysCommonDelInput.getId());
        if (!saveSuccess) {
            return ResultUtils.unknown("删除失败");
        }
        return ResultUtils.success();
    }


}
