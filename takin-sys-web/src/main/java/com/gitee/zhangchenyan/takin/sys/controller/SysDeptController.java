package com.gitee.zhangchenyan.takin.sys.controller;


import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import com.gitee.zhangchenyan.takin.auth.AuthCheckLogin;
import com.gitee.zhangchenyan.takin.auth.AuthCheckPermission;
import com.gitee.zhangchenyan.takin.common.result.Result;
import com.gitee.zhangchenyan.takin.common.result.ResultUtils;
import com.gitee.zhangchenyan.takin.common.utils.Tree;
import com.gitee.zhangchenyan.takin.sys.dto.*;
import com.gitee.zhangchenyan.takin.sys.entity.SysDept;
import com.gitee.zhangchenyan.takin.sys.entity.SysRole;
import com.gitee.zhangchenyan.takin.sys.page.SysDeptPageParam;
import com.gitee.zhangchenyan.takin.sys.service.ISysDeptService;
import com.gitee.zhangchenyan.takin.service.PageResult;
import com.gitee.zhangchenyan.takin.service.client.ServiceClient;

import java.util.List;

/**
 * <p>
 * 部门 前端控制器
 * </p>
 *
 * @author zl
 * @since 2022-02-25
 */
@RestController
@RequestMapping("/takin_api/v1/sys/dept")
@Api(tags = "部门管理")
public class SysDeptController {

    private final ISysDeptService sysDeptService = ServiceClient.create(ISysDeptService.class);

    @AuthCheckLogin
    @AuthCheckPermission("sys:dept:add")
    @PostMapping("add")
    @ApiOperation("添加")
    public Result add(@RequestBody SysDeptAddInput sysDeptAddInput) {
        boolean saveSuccess = sysDeptService.save(BeanUtil.copyProperties(sysDeptAddInput, SysDept.class));
        if (!saveSuccess) {
            return ResultUtils.unknown("添加失败");
        }
        return ResultUtils.success();
    }

    @AuthCheckLogin
    @AuthCheckPermission("sys:dept:edit")
    @PostMapping("edit")
    @ApiOperation("修改")
    public Result edit(@RequestBody SysDeptEditInput sysDeptEditInput) {
        SysDept sysDept = BeanUtil.copyProperties(sysDeptEditInput, SysDept.class);
        boolean saveSuccess = sysDeptService.updateById(sysDept);
        if (!saveSuccess) {
            return ResultUtils.unknown("修改失败");
        }
        return ResultUtils.success();
    }

    @AuthCheckLogin
    @AuthCheckPermission("sys:dept:del")
    @PostMapping("del")
    @ApiOperation("删除")
    public Result del(@RequestBody SysCommonDelInput sysCommonDelInput) {
        return sysDeptService.del(sysCommonDelInput);
    }

    @AuthCheckLogin
    @AuthCheckPermission("sys:dept:index")
    @PostMapping("getPage")
    @ApiOperation("查询分页")
    public Result<PageResult<SysDept>> getPage(@RequestBody SysDeptPageParam sysDeptPageParam) {
        PageResult<SysDept> sysDeptServicePage = sysDeptService.getPage(sysDeptPageParam);
        return ResultUtils.success(sysDeptServicePage);
    }

    @AuthCheckLogin
    @AuthCheckPermission("sys:dept:index")
    @GetMapping("getTree")
    @ApiOperation("查询树形")
    public Result<List<SysDeptTreeOut>> getTree() {
        List<SysDept> sysDeptList = sysDeptService.list();
        List<SysDeptTreeOut> sysDeptTreeOutList = BeanUtil.copyToList(sysDeptList, SysDeptTreeOut.class);
        Tree<SysDeptTreeOut> tree = new Tree<>();
        List<SysDeptTreeOut> result = tree.getAll(sysDeptTreeOutList);
        return ResultUtils.success(result);
    }

    @AuthCheckLogin
    @AuthCheckPermission("sys:dept:index")
    @GetMapping("getAll")
    @ApiOperation("查询部门")
    public Result<List<SysDept>> getAll() {
        List<SysDept> sysRoleList = sysDeptService.list();
        return ResultUtils.success(sysRoleList);
    }

}

