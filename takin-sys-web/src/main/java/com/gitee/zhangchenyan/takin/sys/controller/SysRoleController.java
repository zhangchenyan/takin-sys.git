package com.gitee.zhangchenyan.takin.sys.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.gitee.zhangchenyan.takin.auth.AuthCheckLogin;
import com.gitee.zhangchenyan.takin.auth.AuthCheckPermission;
import com.gitee.zhangchenyan.takin.common.result.Result;
import com.gitee.zhangchenyan.takin.common.result.ResultUtils;
import com.gitee.zhangchenyan.takin.service.PageResult;
import com.gitee.zhangchenyan.takin.service.client.ServiceClient;
import com.gitee.zhangchenyan.takin.sys.dto.*;
import com.gitee.zhangchenyan.takin.sys.entity.SysDept;
import com.gitee.zhangchenyan.takin.sys.entity.SysDic;
import com.gitee.zhangchenyan.takin.sys.entity.SysRole;
import com.gitee.zhangchenyan.takin.sys.page.SysDeptPageParam;
import com.gitee.zhangchenyan.takin.sys.page.SysRolePageParam;
import com.gitee.zhangchenyan.takin.sys.service.ISysDeptService;
import com.gitee.zhangchenyan.takin.sys.service.ISysMenuService;
import com.gitee.zhangchenyan.takin.sys.service.ISysRoleService;
import com.gitee.zhangchenyan.takin.sys.service.ISysRolesMenusService;

import java.util.List;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author zl
 * @since 2022-04-10
 */
@RestController
@Api(tags = "角色管理")
@RequestMapping("/takin_api/v1/sys/role")
public class SysRoleController {
    private final ISysRoleService service = ServiceClient.create(ISysRoleService.class);
    private final ISysRolesMenusService sysRolesMenusService = ServiceClient.create(ISysRolesMenusService.class);


    @AuthCheckLogin
    @AuthCheckPermission("sys:role:add")
    @PostMapping("add")
    @ApiOperation("添加")
    public Result add(@RequestBody SysRoleAddInput sysRoleAddInput) {
        return service.addRoleAndRoleMenu(sysRoleAddInput);
    }

    @AuthCheckLogin
    @AuthCheckPermission("sys:role:edit")
    @PostMapping("edit")
    @ApiOperation("修改")
    public Result edit(@RequestBody SysRoleEditInput sysRoleEditInput) {
        return service.editRoleAndRoleMenu(sysRoleEditInput);
    }

    @AuthCheckLogin
    @AuthCheckPermission("sys:role:del")
    @PostMapping("del")
    @ApiOperation("删除")
    public Result del(@RequestBody SysCommonDelInput commonDelInput) {
        return service.removeByIds(commonDelInput);
    }

    @AuthCheckLogin
    @AuthCheckPermission("sys:role:index")
    @PostMapping("getPage")
    @ApiOperation("查询分页")
    public Result<PageResult<SysRole>> getPage(@RequestBody SysRolePageParam pageParam) {
        PageResult<SysRole> pageResult = service.getPage(pageParam);
        return ResultUtils.success(pageResult);
    }

    @AuthCheckLogin
    @AuthCheckPermission("sys:role:index")
    @GetMapping("getMenuByRoleId")
    @ApiOperation("根据roleId查询menuId")
    public Result<List<Long>> getMenuByRoleId(Long roleId) {
        return sysRolesMenusService.getMenuByRoleId(roleId);
    }

    @AuthCheckLogin
    @AuthCheckPermission("sys:role:index")
    @GetMapping("getAll")
    @ApiOperation("查询角色")
    public Result<List<SysRole>> getAll() {
        List<SysRole> sysRoleList = service.list();
        return ResultUtils.success(sysRoleList);
    }

}
