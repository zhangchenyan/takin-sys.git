package com.gitee.zhangchenyan.takin.sys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.gitee.zhangchenyan"})
public class TakinSysWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(TakinSysWebApplication.class, args);
    }

}
