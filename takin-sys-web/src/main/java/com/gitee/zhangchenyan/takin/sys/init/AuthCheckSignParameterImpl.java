package com.gitee.zhangchenyan.takin.sys.init;

import com.gitee.zhangchenyan.takin.service.IExceptionLogService;
import org.springframework.stereotype.Component;
import com.gitee.zhangchenyan.takin.auth.AuthCheckSignParameter;

/**
 * @Deacription takin验证签名的key
 * @Author zl
 * @Date 2022/4/10 14:20
 * @Version 1.0
 **/
@Component
public class AuthCheckSignParameterImpl implements AuthCheckSignParameter {
    @Override
    public String getKey() {
        return "Y!64Dq7d@OIBVLclBH%fdVKS0mwxyCsCICperKCQwMIPOBcELq*Dkx8PL5p5LBXR";
    }
}
