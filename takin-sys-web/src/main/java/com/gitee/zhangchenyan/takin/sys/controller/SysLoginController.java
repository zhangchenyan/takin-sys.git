package com.gitee.zhangchenyan.takin.sys.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.ShearCaptcha;
import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.gitee.zhangchenyan.takin.auth.AuthCheckLogin;
import com.gitee.zhangchenyan.takin.auth.AuthCheckPermission;
import com.gitee.zhangchenyan.takin.auth.AuthUserIdentity;
import com.gitee.zhangchenyan.takin.auth.AuthUserService;
import com.gitee.zhangchenyan.takin.cache.IJedisClient;
import com.gitee.zhangchenyan.takin.common.result.Result;
import com.gitee.zhangchenyan.takin.common.result.ResultUtils;
import com.gitee.zhangchenyan.takin.common.utils.PassWordUtils;
import com.gitee.zhangchenyan.takin.service.client.ServiceClient;
import com.gitee.zhangchenyan.takin.sys.dto.SysMenuRouteOut;
import com.gitee.zhangchenyan.takin.sys.dto.SysUserLoginInput;
import com.gitee.zhangchenyan.takin.sys.entity.SysUser;
import com.gitee.zhangchenyan.takin.sys.service.ISysMenuService;
import com.gitee.zhangchenyan.takin.sys.service.ISysRoleService;
import com.gitee.zhangchenyan.takin.sys.service.ISysRolesMenusService;
import com.gitee.zhangchenyan.takin.sys.service.ISysUserService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @Deacription 登录控制器
 * @Author zl
 * @Date 2022/4/19 21:24
 * @Version 1.0
 **/
@RestController
@Api(tags = "登录管理")
@RequestMapping("/takin_api/v1/sys/login")
public class SysLoginController {

    private final AuthUserService authUserService;
    private final IJedisClient jedisClient;
    private final ISysUserService sysUserService = ServiceClient.create(ISysUserService.class);
    private final ISysMenuService sysMenuService = ServiceClient.create(ISysMenuService.class);
    private final ISysRolesMenusService sysRolesMenusService = ServiceClient.create(ISysRolesMenusService.class);
    private final ISysRoleService sysRoleService = ServiceClient.create(ISysRoleService.class);

    public SysLoginController(AuthUserService authUserService, IJedisClient jedisClient) {
        this.authUserService = authUserService;
        this.jedisClient = jedisClient;
    }

    /**
     * 创建图片代码
     *
     * @param response 响应
     */
    @GetMapping("/createImageCode")
    @ApiOperation(value = "创建图片代码")
    public void createImageCode(String verifyNumber, HttpServletResponse response) throws IOException {
        ShearCaptcha captcha = CaptchaUtil.createShearCaptcha(120, 50, 4, 3);
        String captchaCode = captcha.getCode();
        String key = "captcha_" + verifyNumber;
        jedisClient.set(key, 600L, captchaCode);
        captcha.write(response.getOutputStream());
    }

    @PostMapping("login")
    @ApiOperation("登录")
    public Result<AuthUserIdentity> login(@RequestBody SysUserLoginInput userLoginInput) {

        //判断验证码
        String key = "captcha_" + userLoginInput.getVerifyNumber();
        String captchaCode = jedisClient.get(key);
        if (StrUtil.isEmpty(captchaCode)) {
            return ResultUtils.unknown("验证码过期");
        }
        if (!userLoginInput.getCaptchaCode().equals(captchaCode)) {
            return ResultUtils.unknown("验证码错误");
        }
        SysUser queryUser = new SysUser();
        queryUser.setUsername(userLoginInput.getUsername());
        SysUser user = sysUserService.getOne(queryUser);
        if (user == null)
            return ResultUtils.unknown("账户不存在");
        boolean verifyPass = PassWordUtils.getVerifyPass(userLoginInput.getPassword(), user.getPassword());
        if (!verifyPass) {
            return ResultUtils.unknown("密码错误");
        }
        user.setPassword("");
        AuthUserIdentity userIdentity = authUserService.login(user);
        return ResultUtils.success(userIdentity);
    }

    @AuthCheckLogin
    @GetMapping("logout")
    @ApiOperation("退出登录")
    public Result logout() {
        authUserService.logout();
        return ResultUtils.success();
    }

    @AuthCheckLogin
    @GetMapping("getUserInfoByUserId")
    @ApiOperation("获取用户信息")
    public Result<SysUser> getUserInfoByUserId() {
        SysUser user = authUserService.getUser(SysUser.class);
        user.setPassword("");
        return ResultUtils.success(user);
    }


    @AuthCheckLogin
    @GetMapping("getBtnByUserId")
    @ApiOperation("获取用户按钮权限")
    public Result<List<String>> getBtnByUserId() {
        SysUser user = authUserService.getUser(SysUser.class);
        List<String> btns = sysRolesMenusService.getBtnByUserId(user.getId());
        return ResultUtils.success(btns);
    }

    @AuthCheckLogin
    @GetMapping("getRoleByUserId")
    @ApiOperation("获取用户角色")
    public Result<List<String>> getRoleByUserId() {
        SysUser user = authUserService.getUser(SysUser.class);
        List<String> codes = sysRoleService.getRoleByUserId(user);
        return ResultUtils.success(codes);
    }

    @AuthCheckLogin
    @GetMapping("getRoute")
    @ApiOperation("查询所以路由")
    public Result<List<SysMenuRouteOut>> getRoute() {
        List<SysMenuRouteOut> routes = sysMenuService.getRoutes();
        return ResultUtils.success(routes);
    }
}
