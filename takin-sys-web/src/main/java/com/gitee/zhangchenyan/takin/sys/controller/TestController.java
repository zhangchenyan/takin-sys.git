package com.gitee.zhangchenyan.takin.sys.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import com.gitee.zhangchenyan.takin.auth.AuthCheckSign;
import com.gitee.zhangchenyan.takin.common.result.Result;
import com.gitee.zhangchenyan.takin.common.result.ResultUtils;
import com.gitee.zhangchenyan.takin.service.client.ServiceClient;
import com.gitee.zhangchenyan.takin.sys.entity.SysUser;
import com.gitee.zhangchenyan.takin.sys.service.ITestService;

@RestController
@Api(tags = "测试管理")
@RequestMapping("/takin_api/v1/sys/test")
@Slf4j
public class TestController {
    private final ITestService testService = ServiceClient.create(ITestService.class);


    @GetMapping("test1")
    @ApiOperation("测试1")
    public Result test1( Long id) {
        SysUser sysUser = testService.getById(id);
        return ResultUtils.success(sysUser.getNickName());
    }

    @GetMapping("testException")
    @ApiOperation("测试异常")
    public Result testException( Long id) {
        testService.test();
        return ResultUtils.success();
    }

    @AuthCheckSign
    @PostMapping("test2")
    @ApiOperation("测试签名")
    public Result test2(@RequestBody SysUser sysUser) {
        return ResultUtils.success(sysUser);
    }

}
