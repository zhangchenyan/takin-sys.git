package com.gitee.zhangchenyan.takin.sys.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import com.gitee.zhangchenyan.takin.auth.AuthCheckLogin;
import com.gitee.zhangchenyan.takin.auth.AuthCheckPermission;
import com.gitee.zhangchenyan.takin.auth.AuthUserIdentity;
import com.gitee.zhangchenyan.takin.auth.AuthUserService;
import com.gitee.zhangchenyan.takin.cache.IJedisClient;
import com.gitee.zhangchenyan.takin.common.result.Result;
import com.gitee.zhangchenyan.takin.common.result.ResultUtils;
import com.gitee.zhangchenyan.takin.common.utils.PassWordUtils;
import com.gitee.zhangchenyan.takin.service.PageResult;
import com.gitee.zhangchenyan.takin.sys.dto.*;
import com.gitee.zhangchenyan.takin.sys.entity.SysDept;
import com.gitee.zhangchenyan.takin.sys.entity.SysUser;
import com.gitee.zhangchenyan.takin.sys.page.SysDeptPageParam;
import com.gitee.zhangchenyan.takin.sys.page.SysUserPageParam;
import com.gitee.zhangchenyan.takin.sys.service.ISysMenuService;
import com.gitee.zhangchenyan.takin.sys.service.ISysUserService;
import com.gitee.zhangchenyan.takin.service.client.ServiceClient;

/**
 * <p>
 * 系统用户 前端控制器
 * </p>
 *
 * @author zl
 * @since 2022-02-25
 */
@RestController
@Api(tags = "用户管理")
@RequestMapping("/takin_api/v1/sys/user")
public class SysUserController {

    private final ISysUserService sysUserService = ServiceClient.create(ISysUserService.class);

    @AuthCheckLogin
    @AuthCheckPermission("sys:user:add")
    @PostMapping("add")
    @ApiOperation("添加")
    public Result add(@RequestBody SysUserAddInput sysUserAddInput) {
        String encryptPass = PassWordUtils.getEncryptPass(sysUserAddInput.getPassword());
        sysUserAddInput.setPassword(encryptPass);
        SysUser sysUser = BeanUtil.copyProperties(sysUserAddInput, SysUser.class);
        boolean saveSuccess = sysUserService.save(sysUser);
        if (!saveSuccess) {
            return ResultUtils.unknown("添加失败");
        }
        return ResultUtils.success();
    }

    @AuthCheckLogin
    @AuthCheckPermission("sys:user:edit")
    @PostMapping("edit")
    @ApiOperation("修改")
    public Result edit(@RequestBody SysUserEditInput editInput) {
        String encryptPass = PassWordUtils.getEncryptPass(editInput.getPassword());
        editInput.setPassword(encryptPass);
        SysUser sysUser = BeanUtil.copyProperties(editInput, SysUser.class);
        boolean saveSuccess = sysUserService.updateById(sysUser);
        if (!saveSuccess) {
            return ResultUtils.unknown("修改失败");
        }
        return ResultUtils.success();
    }

    @AuthCheckLogin
    @AuthCheckPermission("sys:user:del")
    @PostMapping("del")
    @ApiOperation("删除")
    public Result del(@RequestBody SysCommonDelInput sysCommonDelInput) {
        boolean saveSuccess = sysUserService.removeByIds(sysCommonDelInput.getId());
        if (!saveSuccess) {
            return ResultUtils.unknown("删除失败");
        }
        return ResultUtils.success();
    }

    @AuthCheckLogin
    @AuthCheckPermission("sys:user:index")
    @PostMapping("getPage")
    @ApiOperation("查询分页")
    public Result<PageResult<SysUserOut>> getPage(@RequestBody SysUserPageParam pageParam) {
        PageResult<SysUserOut> pageResult = sysUserService.getPageVo(pageParam);
        return ResultUtils.success(pageResult);
    }


}

