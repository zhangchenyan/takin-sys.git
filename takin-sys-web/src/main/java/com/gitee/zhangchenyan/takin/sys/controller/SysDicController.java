package com.gitee.zhangchenyan.takin.sys.controller;

import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.gitee.zhangchenyan.takin.auth.AuthCheckLogin;
import com.gitee.zhangchenyan.takin.auth.AuthCheckPermission;
import com.gitee.zhangchenyan.takin.cache.IJedisClient;
import com.gitee.zhangchenyan.takin.common.result.Result;
import com.gitee.zhangchenyan.takin.common.result.ResultUtils;
import com.gitee.zhangchenyan.takin.service.PageResult;
import com.gitee.zhangchenyan.takin.service.client.ServiceClient;
import com.gitee.zhangchenyan.takin.sys.dto.*;
import com.gitee.zhangchenyan.takin.sys.entity.SysDic;
import com.gitee.zhangchenyan.takin.sys.page.SysDicPageParam;
import com.gitee.zhangchenyan.takin.sys.service.ISysDeptService;
import com.gitee.zhangchenyan.takin.sys.service.ISysDicService;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author zl
 * @since 2022-03-19
 */
@RestController
@Api(tags = "字典管理")
@RequestMapping("/takin_api/v1/sys/dic")
public class SysDicController {
    private final ISysDicService sysDicService = ServiceClient.create(ISysDicService.class);
    private final IJedisClient jedisClient;

    public SysDicController(IJedisClient jedisClient) {

        this.jedisClient = jedisClient;
    }

    @AuthCheckLogin
    @AuthCheckPermission("sys:dic:add")
    @PostMapping("add")
    @ApiOperation("添加")
    public Result add(@RequestBody SysDicAddInput sysDicAddInput) {
        SysDic sysDic = new SysDic();
        BeanUtils.copyProperties(sysDicAddInput, sysDic);
        sysDic.setContent(JSON.toJSONString(sysDicAddInput.getContent()));
        boolean saveSuccess = sysDicService.save(sysDic);
        if (!saveSuccess) {
            return ResultUtils.unknown("添加失败");
        }
        return ResultUtils.success();
    }

    @AuthCheckLogin
    @AuthCheckPermission("sys:dic:edit")
    @PostMapping("edit")
    @ApiOperation("修改")
    public Result edit(@RequestBody SysDicEditInput sysDicEditInput) {
        SysDic sysDic = new SysDic();
        BeanUtils.copyProperties(sysDicEditInput, sysDic);
        sysDic.setContent(JSON.toJSONString(sysDicEditInput.getContent()));
        boolean saveSuccess = sysDicService.updateById(sysDic);
        if (!saveSuccess) {
            return ResultUtils.unknown("修改失败");
        }
        return ResultUtils.success();
    }

    @AuthCheckLogin
    @AuthCheckPermission("sys:dic:del")
    @PostMapping("del")
    @ApiOperation("删除")
    public Result del(@RequestBody SysCommonDelInput commonDelInput) {
        boolean saveSuccess = sysDicService.removeByIds(commonDelInput.getId());
        if (!saveSuccess) {
            return ResultUtils.unknown("删除失败");
        }
        return ResultUtils.success();
    }
    @AuthCheckLogin
    @AuthCheckPermission("sys:dic:index")
    @PostMapping("/getPage")
    @ApiOperation(value = "分页查询")
    public Result<PageResult<SysDicOut>> getPage(@RequestBody SysDicPageParam sysDicPageParam) {
        PageResult<SysDic> result = sysDicService.getPage(sysDicPageParam);
        List<SysDicOut> sysDicOutList = new ArrayList<>();
        for (SysDic sysDic : result.getData()) {
            SysDicOut sysDicOut = new SysDicOut();
            BeanUtils.copyProperties(sysDic, sysDicOut);
            sysDicOut.setContent(JSON.parseArray(sysDic.getContent(), SysDicContent.class));
            sysDicOutList.add(sysDicOut);
        }
        PageResult<SysDicOut> dtoResult = new PageResult<>();
        dtoResult.setPageNumber(result.getPageNumber());
        dtoResult.setPageSize(result.getPageSize());
        dtoResult.setTotalNum(result.getTotalNum());
        dtoResult.setPages(result.getPages());
        dtoResult.setData(sysDicOutList);
        return ResultUtils.success(dtoResult);
    }

}
