package com.gitee.zhangchenyan.takin.sys.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import com.gitee.zhangchenyan.takin.auth.AuthCheckLogin;
import com.gitee.zhangchenyan.takin.auth.AuthCheckPermission;
import com.gitee.zhangchenyan.takin.auth.AuthUserService;
import com.gitee.zhangchenyan.takin.cache.IJedisClient;
import com.gitee.zhangchenyan.takin.common.result.Result;
import com.gitee.zhangchenyan.takin.common.result.ResultUtils;
import com.gitee.zhangchenyan.takin.service.client.ServiceClient;
import com.gitee.zhangchenyan.takin.sys.dto.*;
import com.gitee.zhangchenyan.takin.sys.service.ISysMenuService;

import java.util.List;

/**
 * @Deacription 菜单管理
 * @Author zl
 * @Date 2022/3/12 21:13
 * @Version 1.0
 **/
@RestController
@Api(tags = "菜单管理")
@RequestMapping("/takin_api/v1/sys/menu")
public class SysMenuController {
    private final ISysMenuService sysMenuService = ServiceClient.create(ISysMenuService.class);



    @AuthCheckLogin
    @AuthCheckPermission("sys:menu:index")
    @GetMapping("getMenu")
    @ApiOperation("查询菜单")
    public Result<List<SysMenuTreeOut>> getMenu() {
        List<SysMenuTreeOut> menus = sysMenuService.getMenu();
        return ResultUtils.success(menus);
    }

    @AuthCheckLogin
    @AuthCheckPermission("sys:menu:add")
    @PostMapping("add")
    @ApiOperation("添加")
    public Result add(@RequestBody SysMenuAddInput menuAddInput) {
        sysMenuService.addMenu(menuAddInput);
        return ResultUtils.success();
    }

    @AuthCheckLogin
    @AuthCheckPermission("sys:menu:del")
    @PostMapping("del")
    @ApiOperation("删除")
    public Result del(@RequestBody SysCommonDelInput sysCommonDelInput) {
        return sysMenuService.del(sysCommonDelInput);
    }
}
