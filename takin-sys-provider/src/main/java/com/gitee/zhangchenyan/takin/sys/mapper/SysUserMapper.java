package com.gitee.zhangchenyan.takin.sys.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gitee.zhangchenyan.takin.sys.dto.SysUserOut;
import com.gitee.zhangchenyan.takin.sys.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gitee.zhangchenyan.takin.sys.page.SysUserPageParam;

import java.util.List;

/**
 * <p>
 * 系统用户 Mapper 接口
 * </p>
 *
 * @author zl
 * @since 2022-02-25
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    List<SysUserOut> getPageVo(Page<SysUserOut> page, SysUserPageParam pageParam);
}
