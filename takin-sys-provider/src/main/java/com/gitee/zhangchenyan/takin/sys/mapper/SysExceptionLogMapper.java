package com.gitee.zhangchenyan.takin.sys.mapper;

import com.gitee.zhangchenyan.takin.sys.entity.SysExceptionLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zl
 * @since 2022-05-03
 */
public interface SysExceptionLogMapper extends BaseMapper<SysExceptionLog> {

}
