package com.gitee.zhangchenyan.takin.sys.impl;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gitee.zhangchenyan.takin.sys.entity.SysUser;
import com.gitee.zhangchenyan.takin.sys.service.ISysUserService;
import com.gitee.zhangchenyan.takin.sys.service.ITestService;

@Service
public class TestServiceImpl implements ITestService {
    private final ISysUserService userService;

    public TestServiceImpl(ISysUserService userService) {
        this.userService = userService;
    }

    @Override
    public SysUser getById(Long id) {
        SysUser sysUser = userService.getById(id);
        String nickName = sysUser.getNickName();
        return sysUser;
    }

    @SneakyThrows
    @Override
    public void test() {
        throw new Exception("异常日志测试");
    }
}
