package com.gitee.zhangchenyan.takin.sys.mapper;

import com.gitee.zhangchenyan.takin.sys.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zl
 * @since 2022-03-13
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}
