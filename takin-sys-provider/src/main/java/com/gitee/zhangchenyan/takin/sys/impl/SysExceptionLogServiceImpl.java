package com.gitee.zhangchenyan.takin.sys.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gitee.zhangchenyan.takin.sys.entity.SysExceptionLog;
import com.gitee.zhangchenyan.takin.sys.mapper.SysExceptionLogMapper;
import com.gitee.zhangchenyan.takin.sys.page.SysExceptionLogPageParam;
import com.gitee.zhangchenyan.takin.sys.service.ISysExceptionLogService;
import com.gitee.zhangchenyan.takin.service.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
* <p>
    * 服务实现类
    * </p>
*
* @author zl
* @since 2022-03-13
*/
@Service
public class SysExceptionLogServiceImpl extends BaseServiceImpl<SysExceptionLogMapper, SysExceptionLog, SysExceptionLogPageParam> implements ISysExceptionLogService {

    @Override
    public Wrapper<SysExceptionLog> getPageWrapper(SysExceptionLogPageParam pageParam) {
        LambdaQueryWrapper<SysExceptionLog> queryWrapper = new QueryWrapper<SysExceptionLog>().lambda();
        queryWrapper.like(SysExceptionLog::getName, pageParam.getName());
        return queryWrapper;
    }
}