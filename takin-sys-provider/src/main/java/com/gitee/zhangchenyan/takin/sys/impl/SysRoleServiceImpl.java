package com.gitee.zhangchenyan.takin.sys.impl;

import java.time.LocalDateTime;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import com.gitee.zhangchenyan.takin.common.result.Result;
import com.gitee.zhangchenyan.takin.common.result.ResultUtils;
import com.gitee.zhangchenyan.takin.sys.dto.SysCommonDelInput;
import com.gitee.zhangchenyan.takin.sys.dto.SysRoleAddInput;
import com.gitee.zhangchenyan.takin.sys.dto.SysRoleEditInput;
import com.gitee.zhangchenyan.takin.sys.entity.SysDept;
import com.gitee.zhangchenyan.takin.sys.entity.SysRole;
import com.gitee.zhangchenyan.takin.sys.entity.SysRolesMenus;
import com.gitee.zhangchenyan.takin.sys.entity.SysUser;
import com.gitee.zhangchenyan.takin.sys.mapper.SysRoleMapper;
import com.gitee.zhangchenyan.takin.sys.page.SysRolePageParam;
import com.gitee.zhangchenyan.takin.sys.service.ISysRoleService;
import com.gitee.zhangchenyan.takin.service.BaseServiceImpl;
import org.springframework.stereotype.Service;
import com.gitee.zhangchenyan.takin.sys.service.ISysRolesMenusService;
import com.gitee.zhangchenyan.takin.sys.service.ISysUserService;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zl
 * @since 2022-03-13
 */
@Service
public class SysRoleServiceImpl extends BaseServiceImpl<SysRoleMapper, SysRole, SysRolePageParam> implements ISysRoleService {

    private final ISysRolesMenusService sysRolesMenusService;
    private final ISysUserService sysUserService;

    public SysRoleServiceImpl(ISysRolesMenusService sysRolesMenusService, ISysUserService sysUserService) {
        this.sysRolesMenusService = sysRolesMenusService;
        this.sysUserService = sysUserService;
    }

    @Override
    public Wrapper<SysRole> getPageWrapper(SysRolePageParam pageParam) {
        QueryWrapper<SysRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .like(StrUtil.isNotEmpty(pageParam.getName()), SysRole::getName, pageParam.getName());
        return queryWrapper;
    }

    @Override
    public Result addRoleAndRoleMenu(SysRoleAddInput sysRoleAddInput) {
        SysRole sysRole = BeanUtil.copyProperties(sysRoleAddInput, SysRole.class);
        boolean saveSuccess = save(sysRole);
        if (!saveSuccess) {
            return ResultUtils.unknown("SysRole保存失败");
        }
        List<SysRolesMenus> sysRolesMenusList = new ArrayList<>();
        for (Long menuId : sysRoleAddInput.getMenuIdList()) {
            SysRolesMenus sysRolesMenus = new SysRolesMenus();
            sysRolesMenus.setRoleId(sysRole.getId());
            sysRolesMenus.setMenuId(menuId);
            sysRolesMenusList.add(sysRolesMenus);
        }
        if (sysRolesMenusList.size() > 0) {
            saveSuccess = sysRolesMenusService.saveBatch(sysRolesMenusList);
            if (!saveSuccess) {
                return ResultUtils.unknown("SysRolesMenus保存失败");
            }
        }
        return ResultUtils.success();
    }

    @Override
    public Result editRoleAndRoleMenu(SysRoleEditInput sysRoleEditInput) {
        SysRole sysRole = BeanUtil.copyProperties(sysRoleEditInput, SysRole.class);
        boolean saveSuccess = updateById(sysRole);
        if (!saveSuccess) {
            return ResultUtils.unknown("SysRole修改失败");
        }

        LambdaQueryWrapper<SysRolesMenus> rolesMenusLambdaQueryWrapper = new QueryWrapper<SysRolesMenus>().lambda();
        rolesMenusLambdaQueryWrapper.eq(true, SysRolesMenus::getRoleId, sysRoleEditInput.getId());
        sysRolesMenusService.remove(rolesMenusLambdaQueryWrapper);

        List<SysRolesMenus> sysRolesMenusList = new ArrayList<>();
        for (Long menuId : sysRoleEditInput.getMenuIdList()) {
            SysRolesMenus sysRolesMenus = new SysRolesMenus();
            sysRolesMenus.setRoleId(sysRole.getId());
            sysRolesMenus.setMenuId(menuId);
            sysRolesMenusList.add(sysRolesMenus);
        }
        if (sysRolesMenusList.size() > 0) {
            saveSuccess = sysRolesMenusService.saveBatch(sysRolesMenusList);
            if (!saveSuccess) {
                return ResultUtils.unknown("SysRolesMenus修改失败");
            }
        }

        return ResultUtils.success();
    }

    @Override
    public Result removeByIds(SysCommonDelInput commonDelInput) {
        boolean saveSuccess = removeByIds(commonDelInput.getId());
        if (!saveSuccess) {
            return ResultUtils.unknown("删除失败");
        }
        LambdaQueryWrapper<SysRolesMenus> rolesMenusLambdaQueryWrapper = new QueryWrapper<SysRolesMenus>().lambda();
        rolesMenusLambdaQueryWrapper.in(SysRolesMenus::getRoleId, commonDelInput.getId());
        saveSuccess = sysRolesMenusService.remove(rolesMenusLambdaQueryWrapper);
        if (!saveSuccess) {
            return ResultUtils.unknown("删除失败");
        }
        return ResultUtils.success();
    }

    /**
     * 根据用户ID查询角色集合
     *
     * @param user 用户
     * @return 角色集合
     */
    @Override
    public List<String> getRoleByUserId(SysUser user) {
        LambdaQueryWrapper<SysRole> sysRoleLambdaQueryWrapper = new LambdaQueryWrapper<>();
        sysRoleLambdaQueryWrapper.in(SysRole::getId, user.getRole());
        List<SysRole> sysRoles = list(sysRoleLambdaQueryWrapper);
        List<String> codes = new ArrayList<>();
        sysRoles.forEach(t -> codes.add(t.getCode()));
        return codes;
    }
}