package com.gitee.zhangchenyan.takin.sys;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@MapperScan("com.gitee.zhangchenyan.takin.sys.mapper")
@ComponentScan({"com.gitee.zhangchenyan"})
public class TakinSysProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(TakinSysProviderApplication.class, args);
    }

}
