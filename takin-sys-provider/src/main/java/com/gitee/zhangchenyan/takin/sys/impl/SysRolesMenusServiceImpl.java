package com.gitee.zhangchenyan.takin.sys.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import com.gitee.zhangchenyan.takin.common.result.Result;
import com.gitee.zhangchenyan.takin.common.result.ResultUtils;
import com.gitee.zhangchenyan.takin.common.utils.Tree;
import com.gitee.zhangchenyan.takin.sys.dto.SysDeptTreeOut;
import com.gitee.zhangchenyan.takin.sys.dto.SysMenuTreeOut;
import com.gitee.zhangchenyan.takin.sys.entity.SysMenu;
import com.gitee.zhangchenyan.takin.sys.entity.SysRole;
import com.gitee.zhangchenyan.takin.sys.entity.SysRolesMenus;
import com.gitee.zhangchenyan.takin.sys.entity.SysUser;
import com.gitee.zhangchenyan.takin.sys.mapper.SysRolesMenusMapper;
import com.gitee.zhangchenyan.takin.sys.page.SysRolesMenusPageParam;
import com.gitee.zhangchenyan.takin.sys.service.ISysMenuService;
import com.gitee.zhangchenyan.takin.sys.service.ISysRoleService;
import com.gitee.zhangchenyan.takin.sys.service.ISysRolesMenusService;
import com.gitee.zhangchenyan.takin.service.BaseServiceImpl;
import org.springframework.stereotype.Service;
import com.gitee.zhangchenyan.takin.sys.service.ISysUserService;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zl
 * @since 2022-03-13
 */
@Service
public class SysRolesMenusServiceImpl extends BaseServiceImpl<SysRolesMenusMapper, SysRolesMenus, SysRolesMenusPageParam> implements ISysRolesMenusService {

    private final ISysUserService sysUserService;
    private final ISysMenuService sysMenuService;

    public SysRolesMenusServiceImpl(ISysUserService sysUserService, ISysMenuService sysMenuService) {
        this.sysUserService = sysUserService;
        this.sysMenuService = sysMenuService;
    }

    @Override
    public Wrapper<SysRolesMenus> getPageWrapper(SysRolesMenusPageParam pageParam) {
        return null;
    }

    /**
     * 根据roleId查询menuId
     */
    @Override
    public Result<List<Long>> getMenuByRoleId(Long roleId) {
        LambdaQueryWrapper<SysRolesMenus> queryWrapper = new QueryWrapper<SysRolesMenus>().lambda();
        queryWrapper.eq(SysRolesMenus::getRoleId, roleId);
        List<SysRolesMenus> sysRolesMenusList = list(queryWrapper);
        List<Long> menuIdList = new ArrayList<>();
        for (SysRolesMenus sysRolesMenus : sysRolesMenusList) {
            menuIdList.add(sysRolesMenus.getMenuId());
        }
        return ResultUtils.success(menuIdList);
    }

    /**
     * 根据用户ID查询权限集合
     *
     * @param userId 用户ID
     * @return 权限集合
     */
    @Override
    public List<String> getPermissionsByUserId(Long userId) {
        SysUser sysUser = sysUserService.getById(userId);
        LambdaQueryWrapper<SysRolesMenus> sysRolesMenusLambdaQueryWrapper = new QueryWrapper<SysRolesMenus>().lambda();
        sysRolesMenusLambdaQueryWrapper.in(SysRolesMenus::getRoleId, sysUser.getRole());
        List<SysRolesMenus> sysRolesMenusList = list(sysRolesMenusLambdaQueryWrapper);
        List<Long> menuIdList = new ArrayList<>();
        for (SysRolesMenus sysRolesMenus : sysRolesMenusList) {
            menuIdList.add(sysRolesMenus.getMenuId());
        }
        List<SysMenu> sysMenuList = sysMenuService.listByIds(menuIdList);
        List<String> permissions = new ArrayList<>();
        sysMenuList.forEach(t -> {
            permissions.add(t.getPermission());
        });
        return permissions;
    }

    /**
     * 根据用户ID查询按钮权限集合
     *
     * @param userId 用户ID
     * @return 按钮权限集合
     */
    @Override
    public List<String> getBtnByUserId(Long userId) {
        SysUser sysUser = sysUserService.getById(userId);
        LambdaQueryWrapper<SysRolesMenus> sysRolesMenusLambdaQueryWrapper = new QueryWrapper<SysRolesMenus>().lambda();
        sysRolesMenusLambdaQueryWrapper.in(SysRolesMenus::getRoleId, sysUser.getRole());
        List<SysRolesMenus> sysRolesMenusList = list(sysRolesMenusLambdaQueryWrapper);
        List<Long> menuIdList = new ArrayList<>();
        for (SysRolesMenus sysRolesMenus : sysRolesMenusList) {
            menuIdList.add(sysRolesMenus.getMenuId());
        }
        List<SysMenu> sysMenuList = sysMenuService.listByIds(menuIdList);
        List<String> btns = new ArrayList<>();
        sysMenuList.forEach(t -> {
            if (t.getType() == 3) {
                btns.add(t.getPermission());
            }
        });
        return btns;
    }



}