package com.gitee.zhangchenyan.takin.sys.mapper;

import org.apache.ibatis.annotations.Param;
import com.gitee.zhangchenyan.takin.sys.dto.RolesByMenuIdDto;
import com.gitee.zhangchenyan.takin.sys.entity.SysMenu;
import com.gitee.zhangchenyan.takin.sys.entity.SysRolesMenus;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 角色菜单关联 Mapper 接口
 * </p>
 *
 * @author zl
 * @since 2022-04-15
 */
public interface SysRolesMenusMapper extends BaseMapper<SysRolesMenus> {

    List<SysMenu> getMenusByRoleId(@Param("roleId") long roleId);

    List<RolesByMenuIdDto> getAllRolesByMenuId();
}
