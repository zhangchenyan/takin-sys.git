package com.gitee.zhangchenyan.takin.sys.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gitee.zhangchenyan.takin.common.result.Result;
import com.gitee.zhangchenyan.takin.common.result.ResultUtils;
import com.gitee.zhangchenyan.takin.service.BaseServiceImpl;
import com.gitee.zhangchenyan.takin.sys.dto.SysCommonDelInput;
import com.gitee.zhangchenyan.takin.sys.entity.SysMenu;
import com.gitee.zhangchenyan.takin.sys.mapper.SysDeptMapper;
import com.gitee.zhangchenyan.takin.sys.page.SysDeptPageParam;
import com.gitee.zhangchenyan.takin.sys.entity.SysDept;
import com.gitee.zhangchenyan.takin.sys.service.ISysDeptService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 部门 服务实现类
 * </p>
 *
 * @author zl
 * @since 2022-02-25
 */
@Service
public class SysDeptServiceImpl extends BaseServiceImpl<SysDeptMapper, SysDept, SysDeptPageParam> implements ISysDeptService {

    @Override
    public Result del(SysCommonDelInput sysCommonDelInput) {
        LambdaQueryWrapper<SysDept> queryWrapper = new QueryWrapper<SysDept>().lambda();
        queryWrapper.isNotNull(SysDept::getPid);
        queryWrapper.in(SysDept::getPid, sysCommonDelInput.getId());
        List<SysDept> sysDeptList = list(queryWrapper);
        if (sysDeptList != null && sysDeptList.size() > 0) {
            return ResultUtils.unknown("存在子级不允许删除！");
        }
        boolean isSuccess = removeByIds(sysCommonDelInput.getId());
        if (!isSuccess) {
            return ResultUtils.unknown("删除失败");
        }
        return ResultUtils.success();
    }

    @Override
    public Wrapper<SysDept> getPageWrapper(SysDeptPageParam pageParam) {
        QueryWrapper<SysDept> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .like(StrUtil.isNotEmpty(pageParam.getName()), SysDept::getName, pageParam.getName());
        return queryWrapper;
    }
}
