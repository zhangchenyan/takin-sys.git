package com.gitee.zhangchenyan.takin.sys.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gitee.zhangchenyan.takin.sys.entity.SysDic;
import com.gitee.zhangchenyan.takin.sys.mapper.SysDicMapper;
import com.gitee.zhangchenyan.takin.sys.page.SysDicPageParam;
import com.gitee.zhangchenyan.takin.sys.service.ISysDicService;
import com.gitee.zhangchenyan.takin.service.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zl
 * @since 2022-03-13
 */
@Service
public class SysDicServiceImpl extends BaseServiceImpl<SysDicMapper, SysDic, SysDicPageParam> implements ISysDicService {

    @Override
    public Wrapper<SysDic> getPageWrapper(SysDicPageParam pageParam) {
        QueryWrapper<SysDic> sysDicQueryWrapper = new QueryWrapper<>();
        sysDicQueryWrapper.lambda().like(StrUtil.isNotEmpty(pageParam.getName()), SysDic::getName, pageParam.getName());
        return sysDicQueryWrapper;
    }
}
