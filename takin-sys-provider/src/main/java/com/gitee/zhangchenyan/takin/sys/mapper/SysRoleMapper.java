package com.gitee.zhangchenyan.takin.sys.mapper;

import com.gitee.zhangchenyan.takin.sys.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author zl
 * @since 2022-04-10
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
