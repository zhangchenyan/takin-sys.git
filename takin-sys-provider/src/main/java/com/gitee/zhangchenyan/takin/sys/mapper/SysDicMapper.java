package com.gitee.zhangchenyan.takin.sys.mapper;

import com.gitee.zhangchenyan.takin.sys.entity.SysDic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zl
 * @since 2022-03-19
 */
public interface SysDicMapper extends BaseMapper<SysDic> {

}
