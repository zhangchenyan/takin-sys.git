package com.gitee.zhangchenyan.takin.sys.mapper;

import com.gitee.zhangchenyan.takin.sys.entity.SysDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 部门 Mapper 接口
 * </p>
 *
 * @author zl
 * @since 2022-02-25
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {

}
