package com.gitee.zhangchenyan.takin.sys.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import com.gitee.zhangchenyan.takin.service.BaseServiceImpl;
import com.gitee.zhangchenyan.takin.service.PageResult;
import com.gitee.zhangchenyan.takin.sys.dto.SysUserOut;
import com.gitee.zhangchenyan.takin.sys.page.SysUserPageParam;
import com.gitee.zhangchenyan.takin.sys.entity.SysUser;
import com.gitee.zhangchenyan.takin.sys.mapper.SysUserMapper;
import com.gitee.zhangchenyan.takin.sys.service.ISysUserService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 系统用户 服务实现类
 * </p>
 *
 * @author zl
 * @since 2022-02-25
 */
@Service
public class SysUserServiceImpl extends BaseServiceImpl<SysUserMapper, SysUser, SysUserPageParam> implements ISysUserService {

    private final SysUserMapper sysUserMapper;

    public SysUserServiceImpl(SysUserMapper sysUserMapper) {
        this.sysUserMapper = sysUserMapper;
    }

    @Override
    public Wrapper<SysUser> getPageWrapper(SysUserPageParam pageParam) {
        return null;
    }

    public PageResult<SysUserOut> getPageVo(SysUserPageParam pageParam) {
        long pageNumber = pageParam.getPageNumber();
        if (pageParam.getPageNumberStartIndex() == 0) {
            pageNumber = pageParam.getPageNumber() + 1;
        }
        Page<SysUserOut> page = new Page<>(pageNumber, pageParam.getPageSize());
        List<SysUserOut> dataList = sysUserMapper.getPageVo(page, pageParam);
        PageResult<SysUserOut> pageResult = new PageResult<>();
        pageResult.setPageNumber(pageParam.getPageNumber());
        pageResult.setPageSize(pageParam.getPageSize());
        pageResult.setTotalNum(page.getTotal());
        pageResult.setPages(page.getPages());
        pageResult.setData(dataList);
        return pageResult;
    }
}
